# git-exercise

This is Git exercise for practice.


# Project Structure
```

├── README.md          <- The top-level README for developers using this project.
│   
├── docs               <-  project docs - GIT.dosx; 
│
├── screenshots        <- This folder contain screenshots for this project repo
│
├── siva               <- Contains siva.txt file

```

##  Main Git Commands used for this project

```
# To clone the gitlab locally
git clone git@gitlab.com:siva001/git-exercise.git

# Navigate to downloaded repo
cd git-exercise

# To check the status
git status

# To create a file
touch siva.txt

# To stage the created file on the created folder as per Scenario 1
git add siva/siva.txt

# To commit the staged file
git commit -m "Commit Messages" 

# To push the changes from local to remote repository
git push orgin master

# To create a branch name exercise/conflict-resolution as per Scenario 3
git branch exercise/conflict-resolution
git checkout exercise/conflict-resolution

# To push this newly created branch to remote repo
git push origin exercise/conflict-resolution

# To create a new branch named exercise/diff-checker as per Scenario 4
git branch exercise/diff-checker
git checkout exercise/diff-checker

# To delete a branch locally 
git branch -D exercise/diff-checker

# To delete the branch on remote repo
git push origin --delete exercise/diff-checker

# To create a branch named  exercise/stash-scenario from main branch  as per Scenario 6
git checkout -b exercise/stash-scenario main

# To Tag "1.0.0" as per Scenario 9
git tag 1.0.0

```

## As per Scenario -3 , attaching the screenshot after merging the branch. The content presents in siva.txt file.

<img src="screenshots/b.png" alt="Alt text" title="Merging exercise/conflict-resolution to main">

## As per Scenario - 3, attaching the screenshot of branch merging exercise/conflict-resolution to main branch


<img src="screenshots/a.png" alt="Alt text" title="Merging exercise/conflict-resolution to main">

## As per Scenario - 3 Deleting the branch locally and in Sync with remote

<img src="screenshots/c.png" alt="Alt text" title="Merging exercise/conflict-resolution to main">


## As per Scenario 4, attaching screenshot for creating a new branch named exercise/diff-checker

<img src="screenshots/7.png" alt="Alt text" title="Optional title">


## As per Scenario 4, attaching the screenshots of  siva.txt file presents in Main branch and exercise/diff-checker branch

1. Branch Name : main 
2. File name : siva.txt 

<img src="screenshots/10.png" alt="Alt text" title="Optional title">

1. Branch Name : exercise/diff-checker
2. File name : siva.txt 

<img src="screenshots/9.png" alt="Alt text" title="Optional title">

## As per Scenario 4, Merging the branches

<img src="screenshots/11.png" alt="Alt text" title="Optional title">

## As per Scenario 5, Deleting the branches

<img src="screenshots/13.png" alt="Alt text" title="Optional title">


<img src="screenshots/14.png" alt="Alt text" title="Optional title">

## As per Scenario 6, I am attaching screenshot for Stashing

<img src="screenshots/f.png" alt="Alt text" title="Optional title">


## As per Scenario 7, I am attaching screenshot for Hook merge

<img src="screenshots/15.png" alt="Alt text" title="Optional title">

## As per Scenario 8, I am attaching screenshot for  Github Repository creation

<img src="screenshots/d.png" alt="Alt text" title="Optional title">

## As per Scenario 9, Attaching screenshot for tagging

<img src="screenshots/e.png" alt="Alt text" title="Optional title">

<img src="screenshots/h.png" alt="Alt text" title="Optional title">